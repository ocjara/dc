# Intenta separar los datos en grupo predefinido de grupos asegurando que tengan la misma varianza , 
# genera grupos disjuntos que se consiguen asegurando cada uno de los puntos al centroide mas cercano

from sklearn.cluster import KMeans
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=100000)
newdf = df[["AirTime", "DepDelay"]].dropna()

#newdf = df[["AirTime","Distance","TaxiOut","ArrDelay","DepDelay"]].dropna()

kmeans = KMeans(n_clusters = 4, random_state = 0 , n_jobs = -1).fit(newdf)
kmeans.labels_

np.unique(kmeans.labels_,return_counts=True)




plt.scatter(newdf["AirTime"],newdf["DepDelay"],c= kmeans.labels_)

plt.show()

print(kmeans.cluster_centers_)