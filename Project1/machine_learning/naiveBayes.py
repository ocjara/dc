# Esta enfocado al analisis textual, permite detectar causalidades
# todas sus variables son independientes, funciona bien para suposiciones erroneas
import pandas as pd
import numpy as np
from sklearn.naive_bayes import BernoulliNB # una probabilidad explicita de que ocurra un evento a una probabilidad explicita de que no ocurra
from sklearn.naive_bayes import MultinomialNB # un a probabilidad basada en el numero de veces que ocurre una palabra en un texto anadiendo un parametro de suavisado
from sklearn.naive_bayes import GaussianNB # Datos continuos Asume que las variables regresoras estan distribuidas normalmente , asignando pametros mu y sigma basandonos en una clase concreta

df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=100000)
df = df.sample(frac = 1)
df = df.dropna(subset = ["ArrDelay"])

Y = df["ArrDelay"] > 0

df["Month"] = df["Month"].apply(str)
df["DayofMonth"] = df["DayofMonth"].apply(str)
df["DayOfWeek"] = df["DayOfWeek"].apply(str)
df["TailNum"] = df["TailNum"].apply(str)

X = pd.get_dummies(data = df[['Month','DayofMonth','TailNum','DayOfWeek','Origin','Dest','UniqueCarrier']])

print(X.head())

clf = BernoulliNB
#clf = MultinomialNB

clf.fit(X, Y)

Y_pred = clf.predict(X)

print(np.mean(Y == Y_pred))

print(1 - np.mean(Y))


X = df[['AirTime','Distance','TaxiIn','TaxiOut']] # DepDelay
clf = GaussianNB

clf.fit(X, Y)

Y_pred = clf.predict(X)

print(np.mean(Y == Y_pred))






