# Se explica como un modelo binario, se obtiene los resultados entre [0,1]
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=100000)

df = df.dropna(subset =["ArrDelay"])
df = df.sample(frac = 1).head(100000)
Y = df["ArrDelay"] < 30
X = df[["DepDelay"]]

logreg = LogisticRegression()
logreg.fit(X,Y)
Y_pred = logreg.predict(X)
np.round(logreg.predict_proba(X),3)
print(np.mean(Y_pred == Y))
print(np.mean(Y))

#Filas valores reales, columnas valores predichos
confusion_matrix = confusion_matrix(Y, Y_pred)
print(confusion_matrix)