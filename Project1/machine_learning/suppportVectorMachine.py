# No es intuitivo
# Se trata de un metodo no probabilistico de clasificacion binaria lineal o no lineal
# Divide los grupos de puntos mediante una recta intentando maximixar la distancia de los puntos mas cercanos a la recta con esta por ambos lados
# en caso de no ser separables linealmente, aplica una transformacion del espacio de variables basado en kernel

from sklearn.svm import SVC
import pandas as pd
import numpy as np

df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=500000)

df = df.dropna(subset = ["ArrDelay"])
df = df.sample(frac = 1)
dftest = df.tail(1000)
df = df.head(1000)

X = df[["Distance","AirTime","DepTime","TaxiIn","TaxiOut","DepDelay"]]
X_test = dftest[["Distance","AirTime","DepTime","TaxiIn","TaxiOut","DepDelay"]]

Y = df["ArrDelay"] > 10
Y_test = dftest["ArrDelay"] > 10

clf = SVC(kernel = "linear") # kernel = "linear"
clf.fit(X,Y)

y_pred = clf.predict(X_test)

print(np.mean(y_pred == Y_test))

