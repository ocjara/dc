# es un conjunto de ciento o miles de arboles,
# selecciona aleatoriamente observacuiones de la base de datos
# selecciona aleatoriamente las variables
# VENTAJAS
# Evita sobreajustar los resultados del modelo
# Asigna un peso variable a las distintas observaciones
# Asigna un peso aleatorio a las distintas variables
# RESULTADO FINAL
# media de los resultados obtenidas en los distintos arboles en variables numericas , categoria mayoritaria en las respuestas categoricas

from sklearn import tree
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=500000)

df = df.dropna(subset = ["ArrDelay"])
df = df.sample(frac = 1)
dftest = df.tail(100000)
df = df.head(100000)

clf = tree.DecisionTreeClassifier()

X = df[["Distance","AirTime","DepTime","TaxiIn","TaxiOut","DepDelay"]]
X_test = dftest[["Distance","AirTime","DepTime","TaxiIn","TaxiOut","DepDelay"]]

Y = df["ArrDelay"] > 10
Y_test = dftest["ArrDelay"] > 10

clf = clf.fit(X,Y)
Y_pred_test = clf.predict(X_test)

print(np.mean(Y_test == Y_pred_test))

clf = RandomForestClassifier(n_estimators = 100, n_jobs = -1) # no es bueno generar muchos arboles en algunos casos -1 pide utilizar todos los nucleos del ordenador
clf.fit(X,Y)
Y_pred_test = clf.predict(X_test)

print(clf.feature_importances_)

print(np.mean(Y_test == Y_pred_test))