# Es un modelo matematico utilizado para aproximar las relacion de dependencias entre una variable dependiente y numerica y continua Y
# las variables independientes x asi como un termino aleatorio exilon que corresponde al error o ruido de nuestros modelos
# su objetivo minimizar la distancia cuadratica de todos los puntos en relacion a un arreglo la recta de regresion

# SE DEBE CUMPLIR
# * relacion entre las variables sea lineal esto implica que al incrementar x unidades una de las variables regresoras se incremente en promedio 
# beta veces la variable respuesta 
# * que los errores sean independientes 
# * que los errores tengan varianza constanre(Homocedasticidad)
# * que los errores tengan esperanza matematica igual a 0 
# * que el error total sea la suma de todos los errores
import pandas as pd
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt


df = pd.read_csv("dc/Project1/DataSet/base_datos_2008.csv",nrows=100000)

df = df.dropna(subset =["ArrDelay"])
df = df.sample(frac = 1).head(100000)
Y = df["ArrDelay"]
X = df[["DepDelay"]]

print(df.columns)

regr = linear_model.LinearRegression()
regr.fit(X, Y)
print("Coeficientes: ",regr.coef_)
Y_pred = regr.predict(X)
print("R_cuadrado: ", r2_score(Y, Y_pred))

plt.scatter(X[1:10000], Y[1:10000], color = "black")
plt.plot(X[1:10000], Y_pred[1:10000], color = "blue")

plt.show()
